Name:           perl-Module-Runtime
Version:        0.016
Release:        6
Summary:        Runtime module handling
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Module-Runtime
Source0:        https://cpan.metacpan.org/authors/id/Z/ZE/ZEFRAM/Module-Runtime-%{version}.tar.gz

BuildRequires:  coreutils make perl-generators perl-interpreter
BuildRequires:  perl(:VERSION) >= 5.6 perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(strict) perl(Math::BigInt) perl(warnings) perl(Test::More) >= 0.41


BuildArch:      noarch

%description
The functions exported by this module deal with runtime handling
of Perl modules, which are normally handled at compile time. This
module avoids using any other modules, so that it can be used in
low-level infrastructure

%package_help

%prep
%autosetup -p1 -n Module-Runtime-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=${RPM_BUILD_ROOT}
%{_fixperms} ${RPM_BUILD_ROOT}

%check
make test

%files
%{perl_vendorlib}/Module/

%files help
%doc Changes README
%{_mandir}/man3/Module::Runtime.3*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 0.016-6
- drop useless perl(:MODULE_COMPAT) requirement

* Fri Nov 15 2019 caomeng<caomeng5@huawei.com> - 0.016-5
- Package init
